/**
 * 
 */
package edu.westga.navigation;

import edu.westga.navigation.controller.RoverController;
import edu.westga.navigation.model.Rover;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;

/**
 * Responsible for starting the program to allow the robot to find light.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date Oct 10, 2016
 * @version CS3270 Intelligent Systems
 */
public class Driver {

	/**
	 * Starts the program.
	 *
	 * @precondition : None.
	 * @postcondition : None.
	 * 
	 * @param args
	 *            : Arguments that are not used in this program.
	 */
	public static void main(String[] args) {
		Rover juneBug = new Rover(MotorPort.D, MotorPort.A, SensorPort.S4, SensorPort.S3, SensorPort.S2, SensorPort.S1);
		RoverController bot = new RoverController(juneBug);
		bot.findLight();
	}

}
