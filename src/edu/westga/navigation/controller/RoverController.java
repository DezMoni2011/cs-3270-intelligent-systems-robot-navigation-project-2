/**
 * 
 */
package edu.westga.navigation.controller;

import edu.westga.navigation.model.Direction;
import edu.westga.navigation.model.Rover;
import lejos.hardware.Button;

/**
 * The brains the control an EV3 robot on what to do when trying to find light.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date Oct 10, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public class RoverController {

	private Rover juneBug;

	/**
	 * Constructs a RoverController that helps an EV3 find light.
	 *
	 * @precondition : robot must not be null.
	 * @postcondition : juneBug = robot.
	 * 
	 * @param robot
	 *            the robot to instruct on what to do to find light.
	 */
	public RoverController(Rover robot) {
		if (robot == null) {
			throw new IllegalArgumentException("Robot must not be null.");
		}

		this.juneBug = robot;
	}

	/**
	 * Navigates an EV3 to find a light source.
	 *
	 * @precondition : None
	 *
	 */
	public void findLight() {

		while (!this.juneBug.hasFoundLight()) {

			this.reverseTillCanProceedIfCompleteltBlocked();

			this.turnRightToAvoidLeftObstaclesIfDetected();

			this.turnLeftToAvoidRightObstaclesIfDetected();

			this.determineDirectionToGoIfFrontBlocked();

			System.out.println(this.juneBug.getLight());

			this.juneBug.move(Direction.Forward);

		}

		this.juneBug.stop();
		this.juneBug.beepRepeatively();

		Button.ENTER.waitForPressAndRelease();
	}

	private void determineDirectionToGoIfFrontBlocked() {
		if (this.juneBug.frontBlocked()) {
			this.juneBug.move(Direction.Backward);
			Direction path = this.juneBug.determineBestDirectionToMoveIn(Direction.Left, Direction.Right);
			this.juneBug.move(path);
		}
	}

	private void turnLeftToAvoidRightObstaclesIfDetected() {
		if (this.juneBug.rightBlocked() && !this.juneBug.leftBlocked()) {
			this.juneBug.move(Direction.Left);
		}
	}

	private void turnRightToAvoidLeftObstaclesIfDetected() {
		if (this.juneBug.leftBlocked() && !this.juneBug.rightBlocked()) {
			this.juneBug.move(Direction.Right);
		}
	}

	private void reverseTillCanProceedIfCompleteltBlocked() {
		if (this.juneBug.leftBlocked() && this.juneBug.rightBlocked()) {

			this.juneBug.move(Direction.Backward);
		}
	}

}
