/**
 * 
 */
package edu.westga.navigation.controller;

import edu.westga.navigation.model.Motor;

/**
 * Responsible for controlling the movement and direction of 2 motors.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date Oct 10, 2016
 * @version CS3270 Intelligent Systems
 */
public class Pilot {

	private final int ONE_DEGREE = 5;

	private Motor leftMotor;
	private Motor rightMotor;

	private int speed;

	/**
	 * Creates a Pilot for controlling 2 motors' movements.
	 *
	 * @precondition : leftMotor must not be null, rightMotor must not be null
	 * 
	 * @postcondition : getSpeed() == 0
	 * 
	 * @param ev3LeftMotor
	 *            the left motor of the EV3
	 * @param ev3RightMotor
	 *            the right motor of the EV3
	 */
	public Pilot(Motor ev3LeftMotor, Motor ev3RightMotor) {
		this.leftMotor = ev3LeftMotor;
		this.rightMotor = ev3RightMotor;
		this.speed = 0;
	}

	/**
	 * Moves both motors of the EV3 forward simultaneously.
	 *
	 * @precondition : None
	 *
	 */
	public void move() {
		this.leftMotor.forward();
		this.rightMotor.forward();
	}

	/**
	 * Stops both motors of the EV3 from moving simultaneously.
	 *
	 * @precondition : None
	 *
	 */
	public void stop() {
		this.leftMotor.stop();
		this.rightMotor.stop();
	}

	/**
	 * Moves both motors of the EV3 backward simultaneously.
	 *
	 * @precondition : None
	 *
	 */
	public void backUp() {
		this.leftMotor.backward();
		this.rightMotor.backward();
	}

	/**
	 * Moves both motors in such a way that turns the EV3 left about 45 degrees.
	 *
	 * @precondition : None
	 *
	 */
	public void turnLeft() {
		this.turnLeft(45);
	}

	/**
	 * Moves both motors in such a way that turns the EV3 right about 45
	 * degrees.
	 *
	 * @precondition : None
	 *
	 */
	public void turnRight() {
		turnRight(45);
	}

	/**
	 * Moves both motors in such a way that turns the EV3 left about a specified
	 * degrees.
	 *
	 * @precondition : None
	 *
	 * @param degrees
	 *            : the desired degrees to turn
	 */
	public void turnLeft(int degrees) {
		this.leftMotor.stop(false);
		this.rightMotor.rotate(degrees * ONE_DEGREE);
	}

	/**
	 * Moves both motors in such a way that turns the EV3 right about a
	 * specified degrees.
	 *
	 * @precondition : None
	 *
	 * @param degrees
	 *            : the desired degrees to turn
	 */
	public void turnRight(int degrees) {
		this.rightMotor.stop(false);
		this.leftMotor.rotate(degrees * ONE_DEGREE);

	}

	/**
	 * Sets the speed of both wheels for an EV3
	 *
	 * @precondition : speed >= 0
	 * @postcondition : getSpeed() == speed
	 *
	 * @param speed
	 *            : the desired speed for the motors
	 */
	public void setSpeed(int speed) {
		if (speed < 0) {
			throw new IllegalArgumentException("Speed must be at least 0");
		}

		this.speed = speed;
		this.leftMotor.setSpeed(this.speed);
		this.rightMotor.setSpeed(this.speed);
	}

	/**
	 * Gets the current speed of the EV3.
	 *
	 * @precondition : None.
	 *
	 * @return the speed of the EV3.
	 */
	public int getSpeed() {
		return this.speed;
	}

}
