/**
 * 
 */
package edu.westga.navigation.utility;

import lejos.robotics.SampleProvider;

/**
 * Environment perceptor for an EV3.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date Oct 10, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public class EnvironmentSampler {

	/**
	 * Gets a reading for the specified sensor.
	 *
	 * @precondition : sensor must not be null.
	 *
	 * @param sensor
	 *            : the sensor to read a sample for.
	 * @return the sample read from the sensor.
	 */
	public static float getSampleFrom(SampleProvider sensor) {
		if (sensor == null) {
			throw new IllegalArgumentException("Sensor must not be null when trying to read.");
		}

		float[] sample = new float[sensor.sampleSize()];
		sensor.fetchSample(sample, 0);
		return sample[0];
	}
}
