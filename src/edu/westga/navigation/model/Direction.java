/**
 * 
 */
package edu.westga.navigation.model;


/**
 * Representation of legal directions for and EV3 to travel in.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date Oct 10, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public enum Direction {
	
	Right,
	Left,
	Forward,
	Backward
}
