/**
 * 
 */
package edu.westga.navigation.model;

import edu.westga.navigation.controller.Pilot;
import lejos.hardware.Sound;
import lejos.hardware.port.Port;
import lejos.utility.Delay;

/**
 * Rover robot that is an EV3 with perceptors.
 * 
 * @author Kathryn Browning
 * @author Destiny Harris
 *
 * @date October 10, 2016
 */
public class Rover {

	private final double OFFSET_TURNING_DISTANCE = 0.04;
	private final int INITIAL_SPEED = 350;

	private Motor leftMotor;
	private Motor rightMotor;
	private ObstacleDetector frontDetector;
	private ObstacleDetector leftDetector;
	private ObstacleDetector rightDetector;
	private LightSensor lightSensor;
	private Pilot pilot;

	private Direction travelingDirection;

	/**
	 * Constructs a Rover that is able to move, perceive depth, and detect
	 * colors.
	 * 
	 * @param theLeftMotorPort
	 *            the left motor
	 * @param theRightMotorPort
	 *            the right motor
	 * @param eyeSensorPort
	 *            the ultrasonic sensor
	 * @param theLightSensorPort
	 *            the color sensor
	 */
	public Rover(Port theLeftMotorPort, Port theRightMotorPort, Port theLeftEyeSensor, Port theFrontEyeSensor,
			Port theRightEyeSensor, Port theLightSensorPort) {

		Sound.setVolume(10);

		this.buildPilot(theLeftMotorPort, theRightMotorPort);
		this.initializeDetectors(theFrontEyeSensor, theLeftEyeSensor, theRightEyeSensor);
		this.initializeLightSensor(theLightSensorPort);
	}

	private void buildPilot(Port theLeftMotorPort, Port theRightMotorPort) {
		this.leftMotor = new Motor(theLeftMotorPort);
		this.rightMotor = new Motor(theRightMotorPort);
		this.pilot = new Pilot(this.leftMotor, this.rightMotor);
		this.setSpeed(INITIAL_SPEED);
	}

	private void initializeDetectors(Port theFrontEyeSensor, Port theLeftEyeSensor, Port theRightEyeSensor) {
		this.frontDetector = new ObstacleDetector(theFrontEyeSensor, OFFSET_TURNING_DISTANCE + 0.01, Direction.Forward);
		this.leftDetector = new ObstacleDetector(theLeftEyeSensor, OFFSET_TURNING_DISTANCE, Direction.Left);
		this.rightDetector = new ObstacleDetector(theRightEyeSensor, OFFSET_TURNING_DISTANCE, Direction.Right);
		this.frontDetector.setName("FRONT");
		this.leftDetector.setName("LEFT");
		this.rightDetector.setName("RIGHT");
	}

	private void initializeLightSensor(Port theLightSensorPort) {
		this.lightSensor = new LightSensor(theLightSensorPort);
		this.lightSensor.calibrate();
	}

	/**
	 * Moves safely on the desired path
	 *
	 * @precondition : path != null
	 * @postcondition : getTraveliingDirection() == path.
	 *
	 * @param path
	 *            the direction specified
	 */
	public void safelyMove(Direction path) {
		this.travelingDirection = path;

		switch (this.travelingDirection) {
		case Forward:
			this.goForwardUntilCannotMoveForward();
			break;
		case Left:
			this.goLeftUntilRightSideClear(this.chooseOptimalAngleForTurning());
			break;
		case Right:
			this.goRightUntilLeftSideClear(this.chooseOptimalAngleForTurning());

			break;
		default:
			this.goBackUntilCanMoveForward();
		}
	}

	/**
	 * Moves an Rover down a specified path
	 *
	 * @precondition : path != null
	 * @postcondition : getTraveliingDirection() == path.
	 *
	 * @param path
	 *            the Direction specified
	 */
	public void move(Direction path) {

		this.travelingDirection = path;

		switch (this.travelingDirection) {
		case Forward:
			this.goForward();
			break;
		case Left:
			this.goLeft(this.chooseOptimalAngleForTurning());
			break;
		case Right:
			this.goRight(this.chooseOptimalAngleForTurning());
			break;
		default:
			this.goBack();
		}
	}

	private int chooseOptimalAngleForTurning() {
		if (this.frontBlocked()) {
			return 45;
		}

		return 23;
	}

	/**
	 * Determines whether or not an Rover can move forward.
	 *
	 * @precondition : None
	 *
	 * @return True if an Rover can't move forward, false otherwise
	 */
	public boolean canNotMoveForward() {
		return this.leftBlocked() && this.rightBlocked() || this.frontBlocked();
	}

	/**
	 * Moves an Rover forward
	 *
	 * @precondition : None
	 * @postcondition : an Rover moves forward
	 *
	 */
	public void goForward() {
		this.pilot.move();
	}

	/**
	 * Move an Rover backward
	 *
	 * @precondition : None
	 * @postcondition : an Rover backs up
	 *
	 */
	public void goBack() {
		this.pilot.backUp();
		Delay.msDelay(500);
	}

	/**
	 * Turns an Rover right a specified amount of degrees
	 *
	 * @precondition : None.
	 *
	 * @param degreesToTurn
	 *            The specified degrees to turn right
	 */
	public void goRight(int degreesToTurn) {
		this.pilot.turnRight(degreesToTurn);
	}

	/**
	 * Turns an Rover left a specified amount of degrees
	 *
	 * @precondition : None.
	 *
	 * @param degreesToTurn
	 *            the specified amount of degrees to turn
	 */
	public void goLeft(int degreesToTurn) {
		this.pilot.turnLeft(degreesToTurn);
	}

	/**
	 * Moves an Rover forward until his forward ultrasonic sensor is blocked
	 *
	 * @precondition : None
	 * @postcondition : an Rover moves forward
	 *
	 */
	public void goForwardUntilCannotMoveForward() {
		while (!this.canNotMoveForward()) {
			this.goForward();
		}
	}

	/**
	 * Moves an Rover backwards while his forward ultrasonic sensor is blocked.
	 *
	 * @precondition : None
	 * @postcondition : an Rover moves backward
	 *
	 */
	public void goBackUntilCanMoveForward() {

		while (this.canNotMoveForward()) {
			this.goBack();
		}
	}

	/**
	 * Moves an Rover right a specified amount of degrees until his left side is
	 * away from obstacles.
	 *
	 * @precondition : None.
	 *
	 * @param degreesToTurn
	 *            the specified amount of degrees to turn
	 */
	public void goRightUntilLeftSideClear(int degreesToTurn) {

		while (this.leftBlocked() || this.frontBlocked()) {
			this.goRight(degreesToTurn);
		}
	}

	/**
	 * Moves an Rover left a specified amount of degrees until his right side is
	 * away from obstacles.
	 *
	 * @precondition : None.
	 *
	 * @param degreesToTurn
	 *            the specified amount of degrees to turn
	 */
	public void goLeftUntilRightSideClear(int degreesToTurn) {

		while (this.rightBlocked() || this.frontBlocked()) {
			this.goLeft(degreesToTurn);
		}
	}

	/**
	 * Stops an Rover
	 *
	 * @precondition : None
	 * @postcondition : an Rover stops
	 *
	 */
	public void stop() {
		this.pilot.stop();
	}

	/**
	 * Checks to see if an Rover has found light
	 *
	 * @precondition : None
	 * @postcondition : an Rover has detected light
	 *
	 * @return True if an Rover finds light, false otherwise
	 */
	public boolean hasFoundLight() {
		return this.lightSensor.hasDetectedLight();
	}

	/**
	 * Makes an Rover beep multiple times
	 *
	 * @precondition : None
	 * @postcondition : an Rover beeps
	 *
	 */
	public void beepRepeatively() {
		Sound.beepSequence();
	}

	/**
	 * Checks to see if the left side has any obstacles in its path.
	 *
	 * @precondition : None
	 * @postcondition : None
	 *
	 * @return True if an Rover's left side is blocked, false otherwise
	 */
	public boolean leftBlocked() {
		return this.leftDetector.isBlocked();
	}

	/**
	 * Checks to see if the right side has any obstacles in its path.
	 *
	 * @precondition : None
	 * @postcondition : None
	 *
	 * @return True if an Rover's right side is blocked, false otherwise
	 */
	public boolean rightBlocked() {
		return this.rightDetector.isBlocked();
	}

	/**
	 * Checks to see if the front has any obstacles in front of it.
	 *
	 * @precondition : None
	 * @postcondition : None
	 *
	 * @return True if an Rover's front is blocked, false otherwise
	 */
	public boolean frontBlocked() {
		return this.frontDetector.isBlocked();
	}

	/**
	 * Gets two direction inputs and checks the obstacles present in each
	 * direction. Then decides which path offers the best light and least amount
	 * of obstacles.
	 *
	 * @precondition : directions != null, lightSensor != null
	 * @postcondition : Direction an Rover moves in is changed to be the
	 *                direction that has the most light and least amount of
	 *                obstacles.
	 *
	 * @param someDirection
	 *            the direction an Rover is currently facing
	 * @param anotherDirection
	 *            a direction to compare to an Rover's current direction
	 * @return : The direction that offers a path with fewest obstacles and most
	 *         light
	 */
	public Direction determineBestDirectionToMoveIn(Direction someDirection, Direction anotherDirection) {

		ObstacleDetector someDetector = this.getDetectorFor(someDirection);
		ObstacleDetector anotherDetector = this.getDetectorFor(anotherDirection);

		float someDetectorDistanceAhead = someDetector.getDistanceTillApproachObstacle();
		float anotherDetectorDistanceAhead = anotherDetector.getDistanceTillApproachObstacle();

		int optimalSearchDegree = 45;

		float someDirectionLightRead = 0;
		float anotherDirectionLightRead = 0;

		if (someDirection == Direction.Left) {
			this.goLeft(optimalSearchDegree);
			someDirectionLightRead = this.getLight();
			this.goLeft(optimalSearchDegree * -1);
		} else if (someDirection == Direction.Right) {
			this.goRight(optimalSearchDegree);
			someDirectionLightRead = this.getLight();
			this.goRight(optimalSearchDegree * -1);
		}

		if (anotherDirection == Direction.Left) {
			this.goLeft(optimalSearchDegree);
			anotherDirectionLightRead = this.getLight();
		} else if (anotherDirection == Direction.Right) {
			this.goRight(optimalSearchDegree);
			anotherDirectionLightRead = this.getLight();
		}

		if (anotherDirectionLightRead > someDirectionLightRead) {
			return Direction.Forward;
		} else if (someDirectionLightRead > anotherDirectionLightRead) {

			if (anotherDirection == Direction.Left) {
				this.goLeft(optimalSearchDegree * -1);
			} else if (anotherDirection == Direction.Right) {
				this.goRight(optimalSearchDegree * -1);
			}

			return someDetector.getFacingDirection();
		} else if (someDetectorDistanceAhead > anotherDetectorDistanceAhead) {

			if (anotherDirection == Direction.Left) {
				this.goLeft(optimalSearchDegree * -1);
			} else if (anotherDirection == Direction.Right) {
				this.goRight(optimalSearchDegree * -1);
			}

			return someDetector.getFacingDirection();
		} else {

			if (anotherDirection == Direction.Left) {
				this.goLeft(optimalSearchDegree * -1);
			} else if (anotherDirection == Direction.Right) {
				this.goRight(optimalSearchDegree * -1);
			}

			return anotherDetector.getFacingDirection();
		}
	}

	private ObstacleDetector getDetectorFor(Direction currentDirection) {
		ObstacleDetector detector = null;
		if (currentDirection == Direction.Left) {
			detector = this.leftDetector;
		} else if (currentDirection == Direction.Right) {
			detector = this.rightDetector;
		} else if (currentDirection == Direction.Forward) {
			detector = this.frontDetector;
		}
		return detector;
	}

	/**
	 * Gets the light read by the Rover.
	 *
	 * @precondition : None.
	 * @return the light reading.
	 */
	public float getLight() {
		return this.lightSensor.getLightReading();
	}

	/**
	 * Gets the traveling direction of the Rover.
	 *
	 * @precondition : None.
	 *
	 * @return the direction an Rover is traveling.
	 */
	public Direction getTravelingDirection() {
		return this.travelingDirection;
	}

	/**
	 * Sets the speed for an Rover
	 *
	 * @precondition : speed >= 0
	 * @postcondition : getSpeed() == speed
	 *
	 * @param speed
	 *            : the desired speed for the motors
	 */
	public void setSpeed(int speed) {
		if (speed < 0) {
			throw new IllegalArgumentException("Speed must be at least 0");
		}

		this.pilot.setSpeed(speed);
	}

	/**
	 * Gets the current speed of the Rover.
	 *
	 * @precondition : None.
	 *
	 * @return the speed of the Rover.
	 */
	public int getSpeed() {
		return this.pilot.getSpeed();
	}

}
