/**
 * 
 */
package edu.westga.navigation.model;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;

/**
 * Motor for an EV3.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date Oct 10, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public class Motor extends EV3LargeRegulatedMotor {

	/**
	 * Creates a motor for an EV3.
	 *
	 * @precondition : port must not be null.
	 * @postcondition : None.
	 *
	 * @param port
	 *            : The port to connection point between the sensor and EV3
	 *            brick.
	 */
	public Motor(Port port) {
		super(port);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see lejos.hardware.motor.BaseRegulatedMotor#stop() Stops EV3 and returns
	 * immediately.
	 */
	public void stop() {
		super.stop(true);
	}

}
