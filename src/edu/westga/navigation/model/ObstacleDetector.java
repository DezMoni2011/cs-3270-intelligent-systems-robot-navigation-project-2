/**
 * 
 */
package edu.westga.navigation.model;

import edu.westga.navigation.utility.EnvironmentSampler;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;

/**
 * Obstacle detector for an EV3.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date Oct 10, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public class ObstacleDetector extends EV3UltrasonicSensor {

	private double offset;
	private Direction facingDirection;
	private String name;

	/**
	 * Creates an ObstacleDetector that determines whether an EV3 is near an
	 * obstacle
	 *
	 * @precondition : port must not be null, offset >= 0, facingDirection must
	 *               not be null.
	 * @postcondition : getOffset() == ev3Offset, getFacingDirection ==
	 *                ev3FacingDirection, getName() == ""
	 * 
	 * @param port
	 *            : The port to connection point between the sensor and EV3
	 *            brick.
	 * @param ev3Offset
	 *            the distance from an obstacle at which an EV3 needs to turn
	 * @param ev3FacingDirection
	 *            the direction that the obstacle detector is currently facing
	 *            relative to the EV3.
	 */
	public ObstacleDetector(Port port, double ev3Offset, Direction ev3FacingDirection) {
		super(port);

		if (ev3Offset < 0) {
			throw new IllegalArgumentException("Offset must be at least 0.");
		}
		if (ev3FacingDirection == null) {
			throw new IllegalArgumentException("facing direction must not be null.");
		}

		this.offset = ev3Offset;
		this.facingDirection = ev3FacingDirection;
		this.name = "";
	}

	/**
	 * Gets the distance from an obstacle at which an EV3 needs to turn
	 *
	 * @precondition : None
	 *
	 * @return the offset distance
	 */
	public double getOffset() {
		return offset;
	}

	/**
	 * Gets the direction at which JuneBug is currently facing
	 *
	 * @precondition : None
	 *
	 * @return the current direction JuneBug is facing
	 */
	public Direction getFacingDirection() {
		return facingDirection;
	}

	/**
	 * Determines whether an EV3 path is blocked
	 *
	 * @precondition : None
	 *
	 * @return True iff there's an obstacle in an EV3 path, false otherwise.
	 */
	public boolean isBlocked() {
		return this.getDistanceTillApproachObstacle() < this.getOffset() + 0.01;
	}

	/**
	 * Reads in a sample from the ultrasonic sensors and determines how large of
	 * a distance there is until an obstacle.
	 *
	 * @precondition : None
	 *
	 * @return the distance between an EV3 and an obstacle
	 */
	public float getDistanceTillApproachObstacle() {
		SampleProvider sensor = this.getDistanceMode();
		return EnvironmentSampler.getSampleFrom(sensor);
	}

	/**
	 * Sets the name of an EV3 obstacle detector.
	 *
	 * @precondition : name must not be null.
	 * @postcondition : getName() == name.
	 *
	 * @param name
	 *            : the name of for the obstacle detector.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see lejos.hardware.sensor.BaseSensor#getName() Gets the name of an EV3
	 * obstacle detector.
	 */
	public String getName() {
		return this.name;
	}
}
