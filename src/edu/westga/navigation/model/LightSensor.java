/**
 * 
 */
package edu.westga.navigation.model;

import edu.westga.navigation.utility.EnvironmentSampler;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.utility.Delay;

/**
 * A calibrated light sensor for an EV3.
 *
 * @author Kathyrn Browning
 * @author Destiny Harris
 *
 * @date Oct 10, 2016
 * @version CS3270 Intelligent Systems
 *
 */
public class LightSensor extends EV3ColorSensor {

	private double lightThreshold;

	/**
	 * Creates a light sensor for an EV3.
	 *
	 * @precondition : port must not be null.
	 * @postcondition : getLightThreshold() == 0;
	 *
	 * @param port
	 *            : The port to connection point between the sensor and EV3
	 *            brick.
	 */
	public LightSensor(Port port) {
		super(port);
		this.lightThreshold = 0;
	}

	/**
	 * Determines if the light sensor has found the light source that it's
	 * calibrated for.
	 *
	 * @precondition : None.
	 *
	 * @return true iff the reading is at least the threshold value, otherwise
	 *         false.
	 */
	public boolean hasDetectedLight() {
		return (this.getLightReading() >= this.lightThreshold);
	}

	/**
	 * Reads the current light ambiance of its environment.
	 *
	 * @precondition : None.
	 *
	 * @return the current light ambiance of its environment.
	 */
	public float getLightReading() {
		float ambiance = EnvironmentSampler.getSampleFrom(this.getAmbientMode());

		return ambiance;
	}

	/**
	 * Calibrates the light sensor to an accepted threshold.
	 *
	 * @precondition : None.
	 * @postcondition : lightThreshold = getLightReading();
	 *
	 */
	public void calibrate() {
		Sound.setVolume(10);
		Sound.beepSequenceUp();
		System.out.println("ANY: Start");
		System.out.println("Enter: Stop readings");
		System.out.println("Hold Enter: Accept threshold");
		Button.waitForAnyPress();
		this.askUserToAcceptCurrentLightReading();
		Sound.beepSequence();
		Delay.msDelay(10000);
		System.out.flush();
	}

	/**
	 * Gets the light threshold value of the light sensor.
	 *
	 * @precondition : None.
	 *
	 * @return the light threshold value of the light sensor.
	 */
	public double getLightThreshold() {
		return lightThreshold;
	}

	private void askUserToAcceptCurrentLightReading() {
		this.promptForReadingAcceptance();
		while (Button.ENTER.isUp()) {
			this.promptForReadingAcceptance();
		}
	}

	private void promptForReadingAcceptance() {
		this.displayLightReadingsUntilPress();
		this.lightThreshold = this.getLightReading();
		System.out.println("Is " + this.getLightThreshold() + " OK?");
		Delay.msDelay(5000);
	}

	private void displayLightReadingsUntilPress() {
		while (Button.ENTER.isUp()) {
			this.displayReadings();
		}
	}

	private void displayReadings() {
		System.out.flush();
		System.out.println("Ambiance: " + this.getLightReading());
	}
}
